// const openModalButtons = document.querySelectorAll('[data-modal-target]')
const closeModalButtons = document.querySelectorAll('[data-close-button]')
const overlay = document.getElementById('overlay')
const popup = document.querySelector('.migration-popup');
const urlSearchParams = new URLSearchParams(window.location.search);
const timeToLeave = 24*60*60*1000;
openModal(popup);
// if (document.readyState === 'complete') {
//     openModal(popup);
// } else {
//     document.addEventListener('readystatechange', () => openModal(popup));
// }

// openModalButtons.forEach(button => {
//     button.addEventListener('click', () => {
//         const modal = document.querySelector(button.dataset.modalTarget)
//         openModal(modal)
//     })
// })

// overlay.addEventListener('click', () => {
//     const modals = document.querySelectorAll('.modal.active')
//     modals.forEach(modal => {
//         closeModal(modal)
//     })
// })

closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.modal')
        closeModal(modal)
    })
})

function openModal(modal) {
    if (modal == null) return
    if (urlSearchParams.get('debug') === 'true') {
        let value = getWithExpiry("showLmsMigrationPopup")
        if (value === false) return
        if (value === null) {
            setWithExpiry("showLmsMigrationPopup", true, timeToLeave)
        }
        value = getWithExpiry("showLmsMigrationPopup")
        if (value) {
            modal.classList.add('active')
            overlay.classList.add('active')
        }
    }
}

function closeModal(modal) {
    if (modal == null) return
    modal.classList.remove('active')
    overlay.classList.remove('active')
    setWithExpiry("showLmsMigrationPopup", false, timeToLeave)
}

function setWithExpiry(key, value, ttl) {
    const now = new Date()
    const item = {
        value: value,
        expiry: now.getTime() + ttl,
    }
    localStorage.setItem(key, JSON.stringify(item))
}

function getWithExpiry(key) {
    const itemStr = localStorage.getItem(key)

    if (!itemStr) {
        return null
    }

    const item = JSON.parse(itemStr)
    const now = new Date()

    // compare the expiry time of the item with the current time
    if (now.getTime() > item.expiry) {
        localStorage.removeItem(key)
        return null
    }
    return item.value
}